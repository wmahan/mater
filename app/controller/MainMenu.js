/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global console: false */

Ext.require('Ext.toolbar.Spacer');
Ext.define('mater.controller.MainMenu', {
    extend: 'Ext.app.Controller',

    views: ['MainMenu', 'Settings'],

    models: ['Settings'],
    //stores: ['Settings'],

    refs: [
        {
            ref: 'mainmenu',
            selector: 'mainmenu'
        },
        /*{
            ref: 'showconsole',
            selector: 'mainmenu checkbox[id=showConsoleCheckbox]'
        },*/
        { ref: 'console', selector: 'mainconsole' },
        { ref: 'chat', selector: 'chat' },
        { ref: 'channels', selector: 'channels' },
        { ref: 'news', selector: 'news' },
        { ref: 'ics', selector: 'ics' },
        { ref: 'settings', selector: 'settings' },
    ],

    init: function() {
        "use strict";
        this.control({
            'settings [id=saveSettings]': {
                click: function(cb) {
                    var form = this.getSettings().down('form');
                    var model = form.getRecord();
                    console.assert(model === mater.settings);
                    form.getForm().updateRecord(model);
                    model.save({
                        callback: function(model) {
                            this.getSettings().hide();
                        },
                        scope: this
                    });
                }
            },
            'settings [id=cancelSettings]': {
                click: function(cb) {
                    this.getSettings().hide();
                }
            },
            'mainmenu [id=showSettings]': {
                click: this.showSettings
            },
            'mainmenu [itemId=aboutButton]': {
                click: this.about
            },
            'mainmenu #showConsole': {
                checkchange: function(ci, checked) {
                    if (checked) {
                        this.getConsole().show();
                    }
                    else {
                        this.getConsole().hide();
                    }
                }
            },
            'mainmenu #showChat': {
                checkchange: function(ci, checked) {
                    if (checked) {
                        this.getChat().show();
                    }
                    else {
                        this.getChat().hide();
                    }
                }
            },
            'mainmenu #showChannels': {
                checkchange: function(ci, checked) {
                    if (checked) {
                        this.getChannels().show();
                    }
                    else {
                        this.getChannels().hide();
                    }
                }
            },
            'mainmenu #showNews': {
                checkchange: function(ci, checked) {
                    if (checked) {
                        this.getNews().show();
                    }
                    else {
                        this.getNews().hide();
                    }
                }
            },
            'mainmenu #quit': {
                click: this.quit
            },
            /*'settings': {
                close: function(e) {
                    e.stopPropagation();
                    this.getSettings().hide();
                }
            }*/
        });
        this.application.on({
            icsloggedin: function() {
                this.getMainmenu().show();
            },
            scope: this
        });
    },

    showSettings: function() {
        "use strict";
        var win = this.getSettings();
        win.down('form').loadRecord(mater.settings);
        win.down('tabpanel').setActiveTab(0);
        win.show();
    },

    about: function() {
        // Maybe this belongs in a View, but Ext.MessageBox doesn't
        // seem to support subclasses, so it would be necessary
        // to customize an instance of Ext.window.Window.
        "use strict";
        Ext.MessageBox.show({
            title: 'About',

            msg: '<div id="aboutInfo"><h3>Mater v' + mater.VERSION + '</h3><p>Copyright &copy; 2011-2014  Wil Mahan</p><img style="float: right; margin-left: 10px" src="resources/img/tomato48x48.png" /><p>This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.</p><p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.</p><p>For more information, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p><p>Source code: <a href="https://www.bitbucket.org/wmahan/mater">https://www.bitbucket.org/wmahan/mater</a>.</div>',
            width: 400,
            buttons: Ext.MessageBox.OK,
        });
    },

    quit: function() {
        "use strict";
        this.application.fireEvent('icsquit');
    }
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
