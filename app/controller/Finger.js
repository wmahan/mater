/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.controller.Finger', {
    extend: 'Ext.app.Controller',

    views: ['Finger'],

    fingerRe: /^Finger of ([A-Za-z]+)(\(.*\))*:/,
    init: function() {
        "use strict";
        this.control({
            'button[itemId=closeButton]': {
                click: function(b) {
                    b.up('window').close();
                }
            },
            'panel[itemId=vars]': {
                activate: function(p) {
                    if (!p.loaded) {
                        this.application.fireEvent('sendline',
                            'vars ' + p.up('window').username + '!',
                            function(lines) {
                                p.update(lines.join('\n'));
                                p.loaded = true;
                            });
                    }
                }
            },
            'panel[itemId=ivars]': {
                activate: function(p) {
                    if (!p.loaded) {
                        this.application.fireEvent('sendline',
                            'ivars ' + p.up('window').username + '!',
                            function(lines) {
                                p.update(lines.join('\n'));
                                p.loaded = true;
                            });
                    }
                }
            }
        });
        this.application.on({
            showfinger: function(lines) {
                // TODO: don't display if a window is already open
                // for the same player
                var m, title, txt = lines.join("\n");
                txt = Ext.String.htmlEncode(txt) + "\n";
                txt = mater.controller.Console.linkify(txt);
                m = this.fingerRe.exec(lines[0]);
                if (!m) {
                    window.alert('unexpected finger header: ' + lines[0]);
                    return;
                }
                title = m[2] ? m[1] + m[2] : m[1];
                Ext.widget('finger', {
                    txt: txt,
                    title: title,
                    username: m[1],
                });
            },
            scope: this
        });
    }
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
