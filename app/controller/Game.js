/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global Ext, ChessBoard, console, window, mater */

Ext.require('Ext.layout.container.Border');
Ext.define('mater.controller.Game', {
    extend: 'Ext.app.Controller',

    refs: [
        {ref: 'gamewindow', selector: 'gamewindow'},
    ],
    views: ['GameWindow', 'Clock'],
    models: ['Game'],

    init: function() {
        "use strict";
        var me = this;
        //this.gamemodel = this.getGameModel();
        this.control({
            'gamewindow': {
                afterrender: function(win) {
                    this.win = win;
                    // For some reason there is a JQuery error if the element
                    // is passed to ChessBoard, so pass the ID as a string
                    // instead
                    win.board = new ChessBoard(win.getComponent('cboard').getId(), {
                        draggable: true,
                        dragOffBoard: 'snapback',
                        showNotation: false,
                        onDragStart: this.onDragStart,
                        onDrop: this.onDrop,
                        moveSpeed: 10,
                        win: win,
                        c: this
                    });
                },
                beforeclose: function(win) {
                    if (!win.game.active) {
                        return true;
                    }
                    if (win.game.amPlaying()) {
                        // XXX ask if player wants to resign
                        return false;
                    }
                    if (win.game.relation === mater.model.Game.OBSERVING_EXAMINED ||
                        win.game.relation === mater.model.Game.OBSERVING_PLAYED)
                    {
                        this.application.fireEvent('sendline', 'unobserve ' + win.game.gameID);
                        // don't actually close the window yet
                        return false;
                    }
                    if (win.game.relation === mater.model.Game.EXAMINING) {
                        this.application.fireEvent('sendline', 'unexamine');
                        return false;
                    }
                    return true;
                }
            },
            'panel[itemId=observersTab]': {
                activate: function(p) {
                    if (!p.loaded) {
                        this.application.fireEvent('sendline',
                            'allob ' + p.up('window').game.gameID,
                            function(lines) {
                                // remove "1 game displayed"
                                lines.pop();
                                p.update(lines.join('\n'));
                                p.loaded = true;
                            });
                    }
                }
            },
            'gamewindow #flipButton': {
                click: function(b) {
                    var win = b.up('window');
                    win.flip();
                    me.refreshInfo(win.game);
                }
            },
            'gamewindow #back999Button': {
                click: function(b) {
                    me.application.fireEvent('sendline', 'back 999');
                }
            },
            'gamewindow #backButton': {
                click: function(b) {
                    me.application.fireEvent('sendline', 'back');
                }
            },
            'gamewindow #forwardButton': {
                click: function(b) {
                    me.application.fireEvent('sendline', 'forward');
                }
            },
            'gamewindow #forward999Button': {
                click: function(b) {
                    me.application.fireEvent('sendline', 'forward 999');
                }
            },
            'gamewindow #drawButton': {
                click: function() {
                    me.application.fireEvent('sendline', 'draw');
                }
            },
            'gamewindow #abortButton': {
                click: function() {
                    me.application.fireEvent('sendline', 'abort');
                }
            },
            'gamewindow #adjournButton': {
                click: function() {
                    me.application.fireEvent('sendline', 'adjourn');
                }
            },
            'gamewindow button#closeButton': {
                click: function(b) {
                    var win = b.up('window');
                    win.close();
                }
            },
            'gamewindow button#resignButton': {
                click: function() {
                    me.application.fireEvent('sendline', 'resign');
                }
            }
        });
    },

    onDragStart: function(from, piece, position, orientation) {
        /* jshint camelcase: false */
        "use strict";
        var chess = this.win.game.chess;
        var relation = this.win.game.relation;
        if (!this.win.game.active || chess.game_over() ||
            (chess.turn() === 'w' && piece.search(/^b/) !== -1) ||
            (chess.turn() === 'b' && piece.search(/^w/) !== -1))
        {
            return false;
        }
        if (relation === mater.model.Game.PLAYING_MYMOVE || relation === mater.model.Game.EXAMINING) {
            // playing or examining
            return true;
        }
        else {
            return false;
        }
    },

    onDrop: function(from, to) {
        "use strict";
        var chess = this.win.game.chess;
        if (from === 'offboard' || to === 'offboard') {
            return false;
        }
        // use chess.js to check legality
        var move = chess.move({
            from: from,
            to: to,
            promotion: 'q' // XXX
        });
        if (move === null)  {
            return 'snapback';
        }
        var computerNotation = from + '-' + to;
        if (move.promotion) {
            computerNotation += '=' + move.promotion;
        }
        // Undo the move so that it gets made again when we see
        // the server response. This hack seems necessary because chess.js
        // does not provide a way to check the legality of a move without
        // executing it.
        //game.undo();

        this.win.game.ply += 1;
        this.c.application.fireEvent('sendline', computerNotation);
        this.c.application.fireEvent('sound', 'move');
    },

    msToStr: function(ms) {
        "use strict";
        if (ms < 0) {
            var ret = this.msToStr(-ms);
            if (ret === '0:00') {
                // don't show -0:00
                return ret;
            }
            else {
                return '-' + ret;
            }
        }
        var totSecs = ms / 1000.0;
        var mins = Math.floor(totSecs / 60);
        var secs = Math.round(totSecs % 60);
        if (secs === 60) {
            mins += 1;
            secs = 0;
        }
        if (secs.toString().length === 1) {
            secs = '0' + secs;
        }
        return mins + ':' + secs;
    },

    refreshInfo: function(game) {
        // refresh most of the info about the game in the window
        "use strict";
        var bottomClock = game.win.down('#bottomClock').down('#time');
        var topClock = game.win.down('#topClock').down('#time');
        var topLag = game.win.down('#topClock').down('#lag');
        var bottomLag = game.win.down('#bottomClock').down('#lag');

        if (game.clockTimerId !== null) {
            window.clearTimeout(game.clockTimerId);
            game.clockTimerId = null;
        }
        topClock.removeCls('clockToMove');
        bottomClock.removeCls('clockToMove');

        if ((game.win.board.orientation() === 'black') !== game.win.flipped) {
            game.win.board.orientation(game.win.flipped ? 'black' : 'white');
        }
        console.assert((game.win.board.orientation() === 'black') === game.win.flipped);

        if (game.win.flipped) {
            // black on the bottom
            bottomClock.update(this.msToStr(game.getBlackTime()));
            topClock.update(this.msToStr(game.getWhiteTime()));
            if (!game.isExamined()) {
                bottomLag.update('Lag: ' + this.msToStr(game.lag[0]));
                topLag.update('Lag: ' + this.msToStr(game.lag[1]));
            }
        }
        else {
            // white on the bottom
            topClock.update(this.msToStr(game.getBlackTime()));
            bottomClock.update(this.msToStr(game.getWhiteTime()));
            if (!game.isExamined()) {
                topLag.update('Lag: ' + this.msToStr(game.lag[0]));
                bottomLag.update('Lag: ' + this.msToStr(game.lag[1]));
            }
        }

        var clockToMove;
        if (game.win.flipped) {
            clockToMove = (game.ply % 2 === 0) ? topClock: bottomClock;
        }
        else {
            clockToMove = (game.ply % 2 === 0) ? bottomClock : topClock;
        }
        clockToMove.addCls('clockToMove');
        if (game.clockIsTicking) {
            var time = (game.ply % 2 === 0) ? game.getWhiteTime() : game.getBlackTime();
            game.clockTimerId = Ext.defer(this.updateClock, time % 1000,
                this, [{game: game, ply: game.ply}]);
        }

    },

    update: function(data) {
        "use strict";
        var plyDiff = data.ply - data.game.ply;
        var game = data.game;

        if (!data.fen && plyDiff !== 1 && plyDiff !== 0) {
            console.log('got compressed update for game with plyDiff = ' + plyDiff);
            console.log('data.ply ' + data.ply + ', data.game.ply ' + data.game.ply);
            return;
        }
        if (plyDiff === 0) {
            // A move we made ourselves; board was refreshed; illegal move;
            // bsetup; or wname/bname. Also a delta <d1> board can be
            // sent at the end of the game with plyDiff === 0
            if (data.fen) {
                if (data.fen.split(' ')[0] !== game.win.board.fen()) {
                    game.win.board.position(data.fen);
                }
                if (data.fen.split(' ')[0] !== game.chess.fen().split(' ')[0]) {
                    game.chess.load(data.fen);
                }
            }
        }
        else if (plyDiff === 1) {
            // got a new move
            var mv = game.chess.move(data.san);
            if (!mv) {
                var moves = game.chess.moves();
                for (var mmv in moves) {
                    if (moves.hasOwnProperty(mmv)) {
                        console.log(moves[mmv]);
                    }
                }
                window.alert('failed to execute move: ' + data.san);
                return false;
            }
            console.assert(mv);
            // make the move even if we further update the position below,
            // so it gets animated
            game.win.board.move(mv.from + '-' + mv.to);
            if (mv.flags.search(/[epkq]/i) !== -1) {
                // en passant, promotion, or castling; update the board
                var fen = data.fen;
                if (!fen) {
                    // we did't get the current position, so use
                    // the one in our local model
                    fen = game.chess.fen();
                }
                game.win.board.position(fen);
            }
            this.application.fireEvent('sound', 'move');
        }
        else if (plyDiff < 0) {
            // back/takeback
            game.win.board.position(data.fen);
            game.chess.load(data.fen);
        }
        else if (plyDiff > 0) {
            // started observing after the start of a game or went
            // forward more than one step in an examined game
            game.win.board.position(data.fen);
            game.chess.load(data.fen);
        }
        game.ply = data.ply;
        game.lag[data.ply % 2] += data.lag;
        if (data.fen) {
            // We ignore the value of flip passed with the game update.
            // It is only used to initialize the board.
            game.relation = data.relation;
            game.blackTime = data.blackTime;
            game.whiteTime = data.whiteTime;
            console.assert(data.turn === 'W' || data.turn === 'B');
            console.assert((data.turn === 'W') === (data.ply % 2 === 0));
        }
        else {
            console.assert('remaining' in data);
            if (data.ply % 2) {
                game.whiteTime = data.remaining;
            }
            else {
                game.blackTime = data.remaining;
            }
            /* jshint camelcase: false */
            data.clockIsTicking = data.ply > 1 && !game.chess.game_over();
            if (game.relation === mater.model.Game.PLAYING_MYMOVE) {
                game.relation = mater.model.Game.PLAYING_OPPMOVE;
            }
            else if (game.relation === mater.model.Game.PLAYING_OPPMOVE) {
                game.relation = mater.model.Game.PLAYING_MYMOVE;
            }
        }
        game.clockIsTicking = data.clockIsTicking;
        if (game.clockIsTicking) {
            game.startClock();
        }

        this.refreshInfo(game);
        if (data.san !== 'none') {
            var moveTxt;
            if (data.ply % 2 === 0) {
                moveTxt = (data.ply / 2) + '... ';
            }
            else {
                moveTxt = ((data.ply - 1) / 2 + 1) + '. ';
            }
            moveTxt += data.san;
            if (!game.isExamined()) {
                moveTxt += ' (' + this.msToStr(data.elapsed) + ')';
            }
            game.win.showLastMove(moveTxt);
        }
    },

    updateClock: function(data) {
        "use strict";
        var clock;
        /*if (!data.game.win) {
            console.log('window has gone away');
            return;
        }*/
        if (data.game.ply !== data.ply) {
            console.log('expected ply ' + data.ply + ' but current ply is ' + data.game.ply);
            return;
        }
        var time;
        if (data.game.ply % 2 === 0) {
            time = data.game.getWhiteTime();
            if (data.game.win.flipped) {
                clock = data.game.win.down('#topClock');
            }
            else {
                clock = data.game.win.down('#bottomClock');
            }
        }
        else {
            time = data.game.getBlackTime();
            if (data.game.win.flipped) {
                clock = data.game.win.down('#bottomClock');
            }
            else {
                clock = data.game.win.down('#topClock');
            }
        }
        if (!clock) {
            console.log('could not find clock');
            return;
        }
        clock.down('#time').update(this.msToStr(time));
        var ms = time % 1000;
        if (ms < 100) {
            ms += 1000;
        }
        data.game.clockTimerId = Ext.defer(this.updateClock, ms, this, [data]);
    },

});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
