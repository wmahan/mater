/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.require('Ext.data.proxy.LocalStorage');

Ext.define('mater.model.Settings', {
    extend: 'Ext.data.Model',
    //store: 'mater.store.Settings',
    fields: [
        {name: 'premove',  type: 'boolean', defaultValue: true},
        {name: 'sound',  type: 'boolean', defaultValue: true},
        {name: 'logonScript',  type: 'string',
            defaultValue: 'set seek 0\nxtell %user% Thank you for trying mater %version%.\n'}
    ],

    // Eventually I plan to use an AjaxProxy to store the settings
    // on the server, but that requires authenticating to the server
    // to control access.
    proxy: {
        type: 'localstorage',
        id: 'settings'
    }
});


/* vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
 */
