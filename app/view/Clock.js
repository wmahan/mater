/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.view.Clock', {
	extend: 'Ext.container.Container',
	alias: 'widget.clock',
    layout: 'vbox',
    items: [
        {xtype: 'container', itemId: 'label', cls: 'clockLabel'},
        {xtype: 'container', layout: 'hbox', items:
            [{xtype: 'container', itemId: 'time', cls: 'clockTime'},
                {xtype: 'container', layout: 'vbox',
                    items: [{xtype: 'image', src: 'resources/img/timeseal.png',
                        title: 'Timeseal enabled', cls: 'timesealIcon',
                        itemId: 'timeseal'},
                    {xtype: 'container', itemId: 'lag', cls: 'clockLag'}]}]
        }
    ],
    /*initComponent: function() {
        this.superclass.initComponent.apply(this);
        this.getComponent('label').update(this.labelText);
    }*/
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
