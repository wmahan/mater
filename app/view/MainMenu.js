/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.view.MainMenu', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.mainmenu',
    height: 32,
    hidden: true,
    style: 'padding: 0; margin: 0',
    defaults: {
        scale: 'medium'
    },

    items: [
        {
            text: 'Command',
            menu: [
                {
                    itemId: 'save',
                    text: 'Save games',
                    icon: 'resources/img/save.png',
                    disabled: true
                },
                {
                    itemId: 'quit',
                    iconCls: 'logoutButton',
                    text: 'Log out',
                }
            ]
        },
        ' ',
        {
            text: 'Tools',
            menu: [
                {
                    id: 'showSettings',
                    icon: 'resources/img/settings.png',
                    text: 'Settings...',
                },
                {
                    text: 'Adjudicate',
                    icon: 'resources/img/adjudicate.png',
                    href: 'http://www.freechess.org/Adjudicate',
                },
                {
                    text: 'FICS Games Database',
                    icon: 'resources/img/ficsgames.png',
                    href: 'http://ficsgames.org/',
                },
                {
                    text: 'FICS Home Page',
                    iconCls: 'ficsIcon',
                    href: 'http://www.freechess.org'
                },
            ]
        },
        ' ',
        {
            text: 'Window',
            menu: [{
                xtype: 'menucheckitem',
                itemId: 'showConsole',
                text: 'Console',
                icon: 'resources/img/console.png',
                checked: false
            },
            {
                xtype: 'menucheckitem',
                itemId: 'showChat',
                icon: 'resources/img/24px-speech_bubble.png',
                text: 'Chat window',
                checked: true
            },
            {
                xtype: 'menucheckitem',
                itemId: 'showChannels',
                icon: 'resources/img/channels.png',
                text: 'Channels window',
                checked: true
            },
            {
                xtype: 'menucheckitem',
                itemId: 'showNews',
                icon: 'resources/img/news.png',
                text: 'News window',
                checked: true
            }]
        },
        '->',
        {
            text: 'Help',
            menu: [
                {
                    text: 'Ask for Help',
                    icon: 'resources/img/ask-help.png',
                    itemId: 'askForHelp'
                },
                {
                    text: 'FICS Help Files',
                    icon: 'resources/img/help.png',
                    href: 'http://www.freechess.org/Help'
                },
                {
                    text: 'About',
                    itemId: 'aboutButton',
                    iconCls: 'tomatoIcon'
                },
            ]
        }
    ]
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
