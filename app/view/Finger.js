/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.view.Finger', {
	extend: 'Ext.window.Window',

    alias: 'widget.finger',
    autoShow: true,
    bodyStyle: 'padding: 5px',
    icon: 'resources/img/finger.png',
    defaultFocus: 'closeButton',
    width: 700,
    height: 500,
    items:[
        { xtype: 'tabpanel', items:
            [{title: 'Finger', itemId: 'fingerText', cls: 'fingerText',
                autoScroll: true, height: 390},
            {title: 'Variables', itemId: 'vars', cls: 'fingerText',
                loaded: false,
                autoScroll: true, height: 390},
            {title: 'Interface Variables', itemId: 'ivars', cls: 'fingerText',
                autoScroll: true, height: 390}]
        },
    ],
    buttons: [
        { xtype: 'button', text: 'Close', itemId: 'closeButton' }
    ],
    initComponent: function() {
        "use strict";
        var html;
        if (this.txt) {
            html = mater.controller.Console.linkify(this.txt);
        }
        else {
            html = '<i>Loading...</i>';
        }
        this.items[0].items[0].html = html;
        this.superclass.initComponent.apply(this);
    }
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
