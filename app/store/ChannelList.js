/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.store.ChannelList', {
    extend: 'Ext.data.Store',
    model: 'mater.model.Channel',
    data: [
        // based on http://eboard.sourceforge.net/ics/64.71.131.140.txt
        {"id": "0", "desc": "Admins"},
        {"id": "1", "desc": "Server Help"},
        {"id": "2", "desc": "Discussions about FICS"},
        {"id": "3", "desc": "FICS programmers"},
        {"id": "4", "desc": "Guest Help"},
        {"id": "5", "desc": "SRs"},
        {"id": "6", "desc": "Interface Help"},
        {"id": "7", "desc": "Online Tours"},
        {"id": "10", "desc": "Premove Haters"},
        {"id": "20", "desc": "Team games"},
        {"id": "21", "desc": "Team #1"},
        {"id": "22", "desc": "Team #2"},
        {"id": "23", "desc": "Simuls"},
        {"id": "24", "desc": "Bughouse"},
        {"id": "30", "desc": "Books"},
        {"id": "31", "desc": "Computer Games"},
        {"id": "32", "desc": "Movies"},
        {"id": "33", "desc": "Duck!"},
        {"id": "34", "desc": "Sports"},
        {"id": "35", "desc": "Music"},
        {"id": "36", "desc": "Math/Physics"},
        {"id": "37", "desc": "Philosophy"},
        {"id": "38", "desc": "Literature/Poetry"},
        {"id": "39", "desc": "Politics"},
        {"id": "48", "desc": "Mamer TMs"},
        {"id": "49", "desc": "Mamer Tourneys"},
        {"id": "50", "desc": "Chat"},
        {"id": "51", "desc": "Youth"},
        {"id": "52", "desc": "Elders"},
        {"id": "53", "desc": "Guest Chat"},
        {"id": "55", "desc": "Chess"},
        {"id": "56", "desc": "Beginner Chess"},
        {"id": "57", "desc": "Chess coaching/teaching"},
        {"id": "58", "desc": "Chess Books"},
        {"id": "60", "desc": "Openings"},
        {"id": "61", "desc": "Endgames"},
        {"id": "62", "desc": "Blindfold"},
        {"id": "63", "desc": "CAs"},
        {"id": "64", "desc": "Computer Chess"},
        {"id": "65", "desc": "Events"},
        {"id": "66", "desc": "Examination"},
        {"id": "67", "desc": "Lectures"},
        {"id": "68", "desc": "Ex-Yugoslav"},
        {"id": "69", "desc": "Latin"},
        {"id": "70", "desc": "Finnish"},
        {"id": "71", "desc": "Scandinavian"},
        {"id": "72", "desc": "German"},
        {"id": "73", "desc": "Spanish"},
        {"id": "74", "desc": "Italian"},
        {"id": "75", "desc": "Russian"},
        {"id": "76", "desc": "Dutch"},
        {"id": "77", "desc": "French"},
        {"id": "78", "desc": "Greek"},
        {"id": "79", "desc": "Icelandic"},
        {"id": "80", "desc": "Chinese"},
        {"id": "81", "desc": "Turkish"},
        {"id": "82", "desc": "Portuguese"},
        {"id": "83", "desc": "Computers"},
        {"id": "84", "desc": "Macintosh"},
        {"id": "85", "desc": "Unix/Linux"},
        {"id": "86", "desc": "DOS/Windows"},
        {"id": "87", "desc": "VMS"},
        {"id": "88", "desc": "Programming"},
        {"id": "90", "desc": "STC Bunch"},
        {"id": "91", "desc": "Suicide"},
        {"id": "92", "desc": "Wild"},
        {"id": "93", "desc": "Bughouse Chat"},
        {"id": "94", "desc": "Gambit"},
        {"id": "95", "desc": "Scholastic Chess"},
        {"id": "96", "desc": "College Chess"},
        {"id": "97", "desc": "Crazyhouse"},
        {"id": "98", "desc": "Losers"},
        {"id": "99", "desc": "Atomic"},
        {"id": "100", "desc": "Trivia"},
        {"id": "101", "desc": "TeamLeague"},
        {"id": "175", "desc": "ArchBishops"}
    ]
});

/* vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
 */
