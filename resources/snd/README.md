Sources:

`move.ogg`: I isolated one knock from 
<http://freesound.org/people/110110010/sounds/66397/>

`check.ogg`: This is `Click1.wav` from CClient:
<http://home.centurytel.net/khb/cclient/>
That page says "CClient and its source code have been released into the public domain. The author waives any and all copyright protections."

`tell.ogg` is `tell.wav` from CClient
