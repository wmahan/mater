`timeseal.png`: I created it myself by combining
<https://commons.wikimedia.org/wiki/File:Icon_Clock.svg> and
<https://commons.wikimedia.org/wiki/File:Lock_icon.png> in Gimp

`log_out.png`, `settings.png`, `help.png`, `save.png`, and `news.png`
are from the Tango Desktop project:
<http://tango.freedesktop.org/>

`ficsgames.png` is from the favicon of <http://www.ficsgames.org>

`fics.png` is from the favicon of <http://www.freechess.org>

`adjudicate.png` is based on <http://www.openclipart.org/detail/62989>
by laobc

`finger.png` is based on <https://upload.wikimedia.org/wikipedia/commons/f/ff/Finger-pointing-icon.png>

`channels.png` is from <https://upload.wikimedia.org/wikipedia/commons/3/39/Internet-group-chat.svg>

`console.png` is from <https://commons.wikimedia.org/wiki/File:Nuvola-inspired-terminal.svg>

`observe.png` is from <https://commons.wikimedia.org/wiki/File:Eye_open_font_awesome.svg>

`examine.png` is from <https://commons.wikimedia.org/wiki/File:Black_book_icon.svg>

`forward.png`, `back.png`, `forward999.png`, and `back999.png`
are modified versions of
<https://commons.wikimedia.org/wiki/File:Double-purple-arrow.jpg>

`ask-help.png` is from <https://commons.wikimedia.org/wiki/File:Stock_help-agent.png>

The king icons are from <https://en.wikipedia.org/wiki/File:Chess_klt45.svg> and <https://en.wikipedia.org/wiki/File:Chess_kdt45.svg>

`flip.png` is a modified version of <https://commons.wikimedia.org/wiki/File:Shuffle_icinv.svg>

The chat bubbles are from <https://commons.wikimedia.org/wiki/File:Speech_bubble.svg>

The Tomato icons are from <https://commons.wikimedia.org/wiki/File:Tomato.svg>
- licensed GPLv2 or later

